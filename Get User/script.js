'use strict';

const url = 'https://api.github.com/users/fabpot';
const h1 = document.querySelector('h1');

const createNode = (element) => {
    return document.createElement(element);
};

const setDataUser = (data) => {
    const div = createNode('div');
    const name = createNode('p');
    const email = createNode('p');
    const company = createNode('p');
    const location = createNode('p');
    const img = createNode('img');

    name.textContent = data.name;
    email.textContent = data.email;
    img.src = data.avatar_url;
    company.textContent = data.company;
    location.textContent = data.location;

    location.classList.add('location');
    name.classList.add('name');
    email.classList.add('email');
    company.classList.add('company');

    h1.after(div);
    div.prepend(name);
    div.append(email);
    div.append(img);
    div.append(company);
    div.append(location);
};

const getUser = () => {
    fetch(url)
        .then((response) => response.json())
        .then((data) => setDataUser(data))
        .catch((error) => error);
};

getUser();
