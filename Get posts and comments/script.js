'use strict';

const postsList = document.querySelector('#posts-list');

const createNode = (el) => document.createElement(el);

const setPostsList = (posts) => {
    let arrPost = posts.slice(0, 10);
    arrPost.map((elem) => {
        const li = createNode('li');
        const h3 = createNode('h3');
        const p = createNode('p');

        h3.textContent = `${elem.id} ${elem.title}`;
        p.textContent = elem.body;
        li.dataset.id = elem.id;

        postsList.appendChild(li);
        li.appendChild(h3);
        li.appendChild(p);
        arrPost.push(elem);

        let liList = document.querySelectorAll('li');
        liList.forEach((li) => {
            li.addEventListener('click', handlerClick);
        });
        return;
    });
};

const getPosts = () => {
    fetch('https://jsonplaceholder.typicode.com/posts')
        .then((response) => response.json())
        .then((data) => setPostsList(data))
        .catch((error) => error);
};

const getComments = (idPost, evTarget) => {
    fetch(`https://jsonplaceholder.typicode.com/posts/${idPost}/comments`)
        .then((response) => response.json())
        .then((data) => setCommentList(data, evTarget))
        .catch((error) => error);
};

const setCommentList = (comments, evTarget) => {
    const ulComments = createNode('ul');
    comments.map((el) => {
        const li = createNode('li');
        const h3 = createNode('h3');
        const p = createNode('p');

        h3.textContent = `Name: ${el.name} Email: ${el.email}`;
        p.textContent = el.body;
        evTarget.appendChild(ulComments);
        ulComments.appendChild(li);
        li.appendChild(h3);
        li.appendChild(p);
    });
};

const handlerClick = (event) => {
    let evTarget = event.currentTarget;
    let idPost = parseInt(evTarget.dataset.id);
    return getComments(idPost, evTarget);
};

getPosts();
